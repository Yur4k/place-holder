function multiHolder(formData) {
    let newFormDataArray = [];
    formData.places.forEach(place => {
        newFormDataArray.push({
            ...formData,
            place,
        })
    });

    return newFormDataArray;
}

function prepareForm(formData) {
    return {
        from: formData.from,
        to: formData.to,
        train: formData.train[0],
        wagon_type: formData.train[1],
        date: formData.date,
        wagon_number: formData.wagon_number,
        place: formData.place,
        firstname: "Вася",
        lastname: "Пупкин"
    };
}

module.exports = {
    prepareForm,
    multiHolder
};