const axios = require('axios');
const actions = {
    HOLD: 'reserve',
    REVOCATION: 'cancel',
    ROUTE: 'route',
    SEARCH: 'search',
};

function getDataFromServer(form, action, defaultUrl = 'https://poezd.ua/zd') {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: defaultUrl,
            headers: {
                'Origin':'https://poezd.ua',
                'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
                'Content-Type': 'application/json',
                'cookie': `sa_hash=${Date.now()}shhh${Math.random()};`
            },
            data: {
                'action': action,
                ...form
            }
        }).then(response => {
            resolve({error: 0, data: response.data});
        }).catch(error => {
            reject({error: 1, message: error});
        })
    }).catch(err => err);
}

function getCodeForStation(from, to, date, train) {
    return new Promise((resolve, reject) => {
        axios({
            method: 'post',
            url: 'https://e-kassa.com/booking/get-trainroute.php',
            headers: {
                'Origin':'https://e-kassa.com',
                'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data:`st_from=${from}&st_to=${to}&date=${date}&train=${encodeURI(train)}`,
        }).then(response => {
            resolve({error: 0, data: response.data});
        }).catch(error => {
            reject({error: 1, message: error});
        })
    }).catch(err => err);
}

//@TODO При длительном бронировании множества мест (10...) возможен бан, или что-то похожее

module.exports = {
    getDataFromServer,
    getCodeForStation,
    actions
};