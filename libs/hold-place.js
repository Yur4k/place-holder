const { getDataFromServer, actions } = require('./request-ticket');

const min_sec = (min = 0, sec = 0) => (min * (60 * 1000)) + (sec * 1000);
const delayExe = (func, delay) => setTimeout(func, delay);

async function holdPlace(form) {
    let response = await getDataFromServer(form, actions.HOLD);

    if (!response.error) {
        let data = JSON.parse(response.data.substring(7, response.data.length));
        let reserveId = data.tickets[0].uid;
        let startTime = `${new Date(new Date().getTime() + min_sec(180)).toString().substring(4, 24)}`;
        let endTime = `${new Date(new Date().getTime() + min_sec(180) + min_sec(29, 35)).toString().substring(4, 24)}`;
        console.log(`[${form.place} || ${form.wagon_number}] Билет забронирован: ${startTime}`);
        console.log(`${startTime} --- ${endTime}`);

        setTimeout(async () => {
            console.log(`[${form.place} || ${form.wagon_number}] Снятие брони...`);
            let revocationform = { date: "06.06.2026", uid: reserveId };
            await getDataFromServer(revocationform, actions.REVOCATION);
            holdPlace(form);
        }, min_sec(29, 35)) //min_sec(29, 35)
    } else {
        console.log(`[${form.place} || ${form.wagon_number}]`);
        console.log(response.message.response.data);
        delayExe(() => holdPlace(form), min_sec(0, 3));
    }
}


module.exports = {
    holdPlace
};